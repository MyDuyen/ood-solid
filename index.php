<?php
//example 1. Single responsibility principle

	class People{
		protected $name;
		protected $position;

		function __construct($name,$position){
			$this->name = $name;
			$this->position = $position;
		}

		function getName(){
			return $this->name;
		}

		function getPosition(){
			return $this->position;
		}

		function export(){
			return $this->name+$this->position;
		}
	}

//solve
	class People{
		protected $name;
		protected $position;

		function __construct($name,$position){
			$this->name = $name;
			$this->position = $position;
		}

		function getName(){
			return $this->name;
		}

		function getPosition(){
			return $this->position;
		}
	}

	class Export{
		function export(People $people){
			return $people->name+$people->position;
		}
	}







//emxample 2. Open/closed principle

	class Manager {
		function salaryManager(){
			return 1000000; 
		}
	}

	class Employee {
		function salaryEmployee(){
			return 500000;
		}
	}

	class People{
		protected $name;
		protected $position;

		function __construct($name,$position){
			$this->name = $name;
			$this->position = $position;
		}

		function getName(){
			return $this->name;
		}

		function calcSalary($member){
			if($member == Manager){
				$member->salaryManager();
			}
			else ($member == Employee){
				$member->salaryEmployee();
			}
		}
	}

	class Save{
		function save(People $people){
			return $people->name+$people->position;
		}
	}

//solve

	interface Salary {
    	function salary();
	}

	class Manager implements Salary{
	    function salary(){
	        return 1000000;
	    }
	}

	class Employee implements Salary{
	    function salary(){
	        return 500000;
	    }
	}

	class People{
		protected $name;
		protected $position;

		function __construct($name,$position){
			$this->name = $name;
			$this->position = $position;
		}

		function getName(){
			return $this->name;
		}

	    function calcSalary(Salary $member){
	        return $member->salary();
	    }
	}

	class Save{
		function save(People $people){
			return $people->name+$people->position;
		}
	}





//example 3. Liskov Substitution Principle

	interface Salary {
    	function salary();
	}

	class Manager extends People{
		 function salary(){
	        return 1000000;
	    }
	}

	class Employee extends People{
		function salary(){
	        return 500000;
	    }
	}

	class People{
		protected $name;
		protected $position;

		function __construct($name,$position){
			$this->name = $name;
			$this->position = $position;
		}

		function getName(){
			return $this->name;
		}

		function getPosition(){
			return $this->position;
		}

		function calcSalary(Salary $member){
	        return $member->salary();
	    }
	}

	class Export{
		function export(People $people){
			return $people->name+$people->position;
		}
	}

	class Check{
		protected $people;
		function ____construct(People $people){
			$this->people = $people;
		}

		function checkPosition(){
			$this->people->getPosition();
		}
	}






//example 4. Interface Segregation Principle

	interface Salary {
    	function salary();
    	function manage();
	}

	class Manager implements Salary{
	    function salary(){
	        return 1000000;
	    }

	    function manage(){
	    	return true;
	    }
	}

	class Employee implements Salary{
	    function salary(){
	        return 500000;
	    }

	    function manage(){
	    	return false;
	    }
	}

	class People{
		protected $name;
		protected $position;

		function __construct($name,$position){
			$this->name = $name;
			$this->position = $position;
		}

		function getName(){
			return $this->name;
		}

		function getPosition(){
			return $this->position;
		}

	    function calcSalary(Salary $member){
	        if($member->manage()){
	        	$member->salary();
	        }
	    }
	}	

	class Export{
		function export(People $people){
			return $people->name+$people->position;
		}
	}

	class Check{
		protected $people;
		function ____construct(People $people){
			$this->people = $people;
		}

		function checkPosition(){
			$this->people->getPosition();
		}
	}

//solve

	interface Salary{
		function salary();
	}

	interface Manage{
		function manage();
	}

	class Manager implements Manage,Salary{
	    function salary(){
	        return 1000000;
	    }

	    function manage(){
	    	return true;
	    }
	}

	class Employee implements Salary{
		function salary(){
	        return 500000;
	    }
	}

	class People{
		protected $name;
		protected $position;
		function __construct($name,$position){
			$this->name = $name;
			$this->position = $position;
		}

		function getName(){
			return $this->name;
		}

		function getPosition(){
			return $this->position;
		}

		function calcSalary(Manage $member){
			$member->manage()
		}
	}

	class Export{
		function export(People $people){
			return $people->name+$people->position;
		}
	}

	class Check{
		protected $people;
		function ____construct(People $people){
			$this->people = $people;
		}

		function checkPosition(){
			$this->people->getPosition();
		}
	}





//example 5. Dependency inversion principle

	interface Salary{
		function salary();
	}

	interface Manage{
		function manage();
	}

	class Manager implements Manage,Salary{
	    function salary(){
	        return 1000000;
	    }

	    function manage(){
	    	return true;
	    }
	}

	class Employee implements Salary{
		function salary(){
	        return 500000;
	    }
	}

	class People{
		protected $name;
		protected $position;

		function __construct($name,$position){
			$this->name = $name;
			$this->position = $position;
		}

		function getName(){
			return $this->name;
		}

		function getPosition(){
			return $this->position;
		}

		function calcSalary(Manage $member){
			$member->manage()
		}
	}

	class Export{
		function export(People $people){
			return $people->name+$people->position;
		}
	}

	class Check{
		protected $people;
		function ____construct(People $people){
			$this->people = $people;
		}

		function checkPosition(){
			$this->people->getPosition();
		}
	}

	class Transport{

	}

	class Move{
		private $transport;
		function ____construct(Transport $ransport){
			$this->transport = $transport; 
		}
	}


//solve
	
	interface Salary{
		function salary();
	}

	interface Manage{
		function manage();
	}

	class Manager implements Manage,Salary{
	    function salary(){
	        return 1000000;
	    }

	    function manage(){
	    	return true;
	    }
	}

	class Employee implements Salary{
		function salary(){
	        return 500000;
	    }
	}

	class People{
		protected $name;
		protected $position;

		function __construct($name,$position){
			$this->name = $name;
			$this->position = $position;
		}

		function getName(){
			return $this->name;
		}

		function getPosition(){
			return $this->position;
		}

		function calcSalary(Manage $member){
			$member->manage()
		}
	}

	class Export{
		function export(People $people){
			return $people->name+$people->position;
		}
	}

	class Check{
		protected $people;

		function ____construct(People $people){
			$this->people = $people;
		}

		function checkPosition(){
			$this->people->getPosition();
		}
	}

	interface Transport{
		function move();
	}

	class moveCar implements Transport{
		function move(){

		}
	}

	class moveBus implements Transport{
		function move(){

		}
	}

	class Move{
		private $transport;
		function ____construct(Transport $ransport){
			$this->transport = $transport; 
		}
	}